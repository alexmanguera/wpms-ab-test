=== WP Marketing Suite Filter ===
Author URI: http://www.wpmarketingsuite.com
Tags: gotargeting, content personalisation, site segmentation
Version: 0.41
Requires at least: 3.0
Tested up to: 4.1
Stable tag: 4.1

== Description ==

WP Marketing Suite enables Wordpress publishers to customize content based on the Geolocation information of the reader.  

WP Marketing Suite implements shortcodes that let publishers incorporate reader's geolocation data into posts, and post titles.  Additionally, publishers can use shortcodes in posts which let them tailor post content to the reader based on proximity or location.  This product includes GeoLite data created by MaxMind, available from http://www.maxmind.com/.

Use WP Marketing Suite to customize the content of your blog based on the geographic location of your readers.

**Available Filter Shortcodes**

**This filter content in the article body, title based on the reader's geolocation data:**

* [wpms_ip] - IP Address of the reader
* [wpms_city] - City of the reader
* [wpms_state_code] - Two letter State code of the reader
* [wpms_country_name] - Country name of the reader
* [wpms_country_code] - Two letter Country code of the reader
* [wpms_latitude] - Latitude of the reader
* [wpms_longitude] - Latitude of the reader

**Conditional Shortcodes - Only available in the body of the post:**

* [wpms_is_city_and_state city="Yardley" state_code="PA"]
* [wpms_is_ip" ip="xx.xx.xx.xx"]
* [wpms_is_ips" ip="xx.xx.xx.xx,aa.bb.cc.dd"]
* [wpms_is_not_ip" ip="xx.xx.xx.xx"]
* [wpms_is_not_ips" ip="xx.xx.xx.xx,aa.bb.cc.dd"]
* [wpms_is_city" city=""]
* [wpms_is_cities" cities="city one,city two,city three"]
* [wpms_is_not_city" city=""]
* [wpms_is_not_cities" cities="city one,city two,city three"]
* [wpms_is_nearby"] - Uses the value you specify in the Nearby Range setting from the administrative panel
* [wpms_is_not_nearby"]
* [wpms_is_within" miles="10"] 
* [wpms_is_within kilometers="12"]
* [wpms_is_country_name" country_name=""] 
* [wpms_is_country_names" country_name="United States,Egypt,Albania"] 
* [wpms_is_country_code" country_code=""]
* [wpms_is_country_codes" country_codes="US,GB,AZ"]
* [wpms_is_state_code" state_code=""] 
* [wpms_is_state_codes" state_codes="PA,NJ,TX"] 
* [wpms_is_not_country_name" country_name=""] 
* [wpms_is_not_country_names" country_names="United States,Egypt,Albania"] 
* [wpms_is_not_country_code" country_code=""] 
* [wpms_is_not_country_codes" country_codes="US,GB,AZ"] 
* [wpms_is_not_state_code" state_code=""] 
* [wpms_is_not_state_codes" state_codes="PA,NJ,TX"] 

**Examples**

    [wpms_is_nearby] Hi Neighbor! [/wpms_is_nearby] - Will display "Hi Neighbor!" to readers within a configurable distance from your home base.
    [wpms_is_within miles=10] Come on over! [/wpms_is_within] - Will display "Come on over!" in the post body if the user is viewing the post from within 10 miles.
    [wpms_is_ip ip=123.123.123.123] I used to own this IP Address [/wpms_is_ip] - Will display the message only if the user has that specific IP Address.
    [wpms_is_city city="Yardley"] Hello Fellow Yardlian [/wpms_is_city] - Will display the message only if the user has that specific IP Address.
    [wpms_is_not_cities cities="Yardley,Morrisville"] Glad you don't live in Yardley or Morrisville? [/wpms_is_not_cities] - Will display only when visitor is not viewing the page from Yardley or Morrisville

Configurable options allow you to set a default phrase for each available shortcode in the event that the user was unable to be geo-located.

== Installation ==

1. Upload all the files into your wp-content/plugins directory, be sure to put them into a folder called "wpms" - name is important.
2. Activate the plugin at the plugin administration page
3. Follow set-up steps on main WP Marketing Suite settings page 

Note: You must install the MaxMind GeoLite City Database in order for this plugin to function properly.  

Please see the [WP Marketing Suite plugin home page](http://www.wpmarketingsuite.com/) for details

== Frequently Asked Questions ==

Please visit the [wpms community page](http://www.wpmarketingsuite.com/) for suggestions and help.

== Screenshots ==

1. screenshot-1.png is the administrative options screen.
2. screenshot-2.png is creating a custom shortcode for a visitor segment. (where user agent contains Safari, but not Chrome or FireFox)

== Change Log ==

= 0.41 =
* Custom Shortcodes